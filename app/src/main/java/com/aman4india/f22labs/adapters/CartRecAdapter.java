package com.aman4india.f22labs.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aman4india.f22labs.Database.DbText;
import com.aman4india.f22labs.R;

import java.util.List;


public class CartRecAdapter extends RecyclerView.Adapter<CartRecAdapter.CustomViewHolder> {

    private List<DbText> mTexts;
    private Context context;

    public CartRecAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_cart_items, viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.nametxt.setText(mTexts.get(position).getItemName());
        DbText txt = mTexts.get(position);
        double total = Double.parseDouble(txt.getQty()) * Double.parseDouble(txt.getPrice());
        holder.pricetxt.setText(total + "");
        holder.qtytxt.setText(mTexts.get(position).getPrice() + " ( " + mTexts.get(position).getQty() + " )");
    }

    @Override
    public int getItemCount() {
        if (mTexts != null)
            return mTexts.size();
        else return 0;
    }

    public void setText(List<DbText> texts) {
        mTexts = texts;
        notifyDataSetChanged();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView nametxt, qtytxt, pricetxt;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            nametxt = itemView.findViewById(R.id.nametxt);
            qtytxt = itemView.findViewById(R.id.qtytxt);
            pricetxt = itemView.findViewById(R.id.pricetxt);

        }
    }

}
