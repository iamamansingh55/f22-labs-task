package com.aman4india.f22labs.adapters;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aman4india.f22labs.Database.CartDatabase;
import com.aman4india.f22labs.Database.DatabaseDao;
import com.aman4india.f22labs.Database.DbText;
import com.aman4india.f22labs.R;
import com.aman4india.f22labs.activities.DetailsActivity;
import com.aman4india.f22labs.models.ItemsResponse;
import com.aman4india.f22labs.viewmodel.TextViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.aman4india.f22labs.activities.MainActivity.updatecart;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CustomViewHolder> {
    private static final String TAG = RecyclerAdapter.class.getSimpleName();
    TextViewModel textViewModel;
    Application mainActivity;
    private Context context;
    private List<ItemsResponse> data;


    public RecyclerAdapter(Application mn, Context context, List<ItemsResponse> data, TextViewModel textViewModel) {
        this.context = context;
        this.data = data;
        this.textViewModel = textViewModel;
        mainActivity = mn;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_items, viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, final int position) {
        Picasso.get().load(data.get(position).getImageUrl()).into(holder.itemimage);
        holder.pricetxt.setText("₹ " + data.get(position).getItemPrice());
        holder.ratingtxt.setText(data.get(position).getAverageRating());
        holder.nametxt.setText(data.get(position).getItemName());
        holder.removig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int curItem;
                try {
                    DbText dbText = new DbText();
                    dbText.setItemName(data.get(position).getItemName());
                    dbText.setPrice(data.get(position).getItemPrice());
                } catch (NumberFormatException e) {
                    curItem = 0;
                }

                DatabaseDao directorDao = CartDatabase.getDatabase(context).textDao();
                DbText directorToUpdate = null;
                try {
                    directorToUpdate = directorDao.findItemsByTitle(data.get(position).getItemName());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    curItem = Integer.parseInt(directorDao.getQuantity(data.get(position).getItemName()));
                    curItem -= 1;
                } catch (NumberFormatException e) {
                    curItem = 0;
                }
                DbText dbText = new DbText();
                dbText.setItemName(data.get(position).getItemName());
                dbText.setPrice(data.get(position).getItemPrice());
                dbText.setQty(curItem + "");

                if (directorToUpdate != null) {

//                    if (!directorToUpdate.itemName.equals(data.get(position).getItemName())) {
//                        directorToUpdate.itemName = data.get(position).getItemName();
//                        directorDao.update(directorToUpdate);
//                    }

                    if (curItem == 0) {
                        updatecart.setText("");
                        textViewModel.deleteItem(dbText);
                        Toast.makeText(mainActivity, "Item removed", Toast.LENGTH_SHORT).show();
                    } else {
//                        updatecart.setText(String.valueOf(curItem));
                        Toast.makeText(mainActivity, "Item updated", Toast.LENGTH_SHORT).show();
                        directorDao.update(String.valueOf(curItem), data.get(position).getItemName());
                    }
                }
            }
        });

        holder.addimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int curItem;


                DatabaseDao directorDao = CartDatabase.getDatabase(context).textDao();
                DbText directorToUpdate = null;
                try {
                    directorToUpdate = directorDao.findItemsByTitle(data.get(position).getItemName());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    curItem = Integer.parseInt(directorDao.getQuantity(data.get(position).getItemName()));
                    curItem += 1;
                } catch (NumberFormatException e) {
                    curItem = 1;
                }
                DbText dbText = new DbText();
                dbText.setItemName(data.get(position).getItemName());
                dbText.setPrice(data.get(position).getItemPrice());
                dbText.setQty(curItem + "");

//                if (directorToUpdate != null) {
//                    if (!directorToUpdate.itemName.equals(data.get(position).getItemName())) {
//                        directorToUpdate.itemName = data.get(position).getItemName();
//                        directorDao.update(directorToUpdate);
//                    }
//                }
                if (directorToUpdate == null) {
                    textViewModel.insert(dbText);
                    Toast.makeText(mainActivity, "Item added", Toast.LENGTH_SHORT).show();
                } else {
                    directorDao.update(String.valueOf(curItem), data.get(position).getItemName());
                    Toast.makeText(mainActivity, "Item Updated", Toast.LENGTH_SHORT).show();
                }

//                updatecart.setText(String.valueOf(curItem));
            }
        });

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,DetailsActivity.class);
                intent.putExtra("name",data.get(position).getItemName());
                intent.putExtra("image",data.get(position).getImageUrl());
                intent.putExtra("price",data.get(position).getItemPrice());
                intent.putExtra("rate",data.get(position).getAverageRating());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView itemimage, addimg, removig;
        CardView cardview;
        TextView pricetxt, ratingtxt, nametxt;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            itemimage = itemView.findViewById(R.id.itemimage);
            pricetxt = itemView.findViewById(R.id.pricetxt);
            ratingtxt = itemView.findViewById(R.id.ratingtxt);
            nametxt = itemView.findViewById(R.id.nametxt);
            addimg = itemView.findViewById(R.id.addimg);
            removig = itemView.findViewById(R.id.removimge);
            cardview = itemView.findViewById(R.id.cardview);
        }
    }
}
