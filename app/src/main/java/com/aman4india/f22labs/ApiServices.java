package com.aman4india.f22labs;

import com.aman4india.f22labs.models.ItemsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiServices {

    @GET("data.json")
    Call<List<ItemsResponse>> getItemsResponse();


}
