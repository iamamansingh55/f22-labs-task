package com.aman4india.f22labs.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aman4india.f22labs.Database.DbText;
import com.aman4india.f22labs.R;
import com.aman4india.f22labs.adapters.CartRecAdapter;
import com.aman4india.f22labs.viewmodel.TextViewModel;

import java.util.List;

public class CartActivity extends AppCompatActivity {

    private static final String TAG = CartActivity.class.getSimpleName();
    RecyclerView recyclerView;
    Toolbar toolbar;
    TextViewModel textViewModel;
    CartRecAdapter adapter;
    CheckBox couponcheck;
    RelativeLayout couponlayout;
    TextView grandtotal;
    Button applybtn;
    EditText aplyedt;
    String totalAmount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        toolbar = findViewById(R.id.toolbar);
        textViewModel = ViewModelProviders.of(this).get(TextViewModel.class);
        couponcheck = findViewById(R.id.couponcheck);
        couponlayout = findViewById(R.id.couponlayout);
        grandtotal = findViewById(R.id.grandtotal);
        applybtn = findViewById(R.id.applybtn);
        aplyedt = findViewById(R.id.aplyedt);

        couponcheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    couponlayout.setVisibility(View.VISIBLE);
                } else {
                    grandtotal.setText(totalAmount+"");
                    aplyedt.setText("");
                    couponlayout.setVisibility(View.GONE);
                }
            }
        });
//        totalAmount = grandtotal.getText().toString();
        applybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String coupn = aplyedt.getText().toString().trim();
                double grndtot = Double.parseDouble(totalAmount);
                if (coupn.equals("F22LABS")) {
                    if (grndtot > 400) {
                        double disamt = grndtot - (grndtot * 20) / 100;
                        grandtotal.setText("" + disamt);
                        Toast.makeText(CartActivity.this, "Coupon applied successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CartActivity.this, "Total Ordered Amount should > 400", Toast.LENGTH_SHORT).show();
                    }
                } else if (coupn.equals("FREEDEL")) {

                    if (grndtot > 100) {
                        double disamt = grndtot - 30;
                        grandtotal.setText("" + disamt);
                        Toast.makeText(CartActivity.this, "Coupon applied successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CartActivity.this, "Total Ordered Amount should > 100", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(CartActivity.this));

        adapter = new CartRecAdapter(CartActivity.this);
        recyclerView.setAdapter(adapter);
        textViewModel.getAllTexts().observe(this, new Observer<List<DbText>>() {
            @Override
            public void onChanged(@Nullable final List<DbText> text) {
                adapter.setText(text);
                double total = 0;
                for (DbText txt : text) {
                    total += Double.parseDouble(txt.getQty()) * Double.parseDouble(txt.getPrice());
                }
                grandtotal.setText("₹ " + (total+30));
                totalAmount = String.valueOf((total+30));
            }
        });


    }
}
