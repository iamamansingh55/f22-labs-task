package com.aman4india.f22labs.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aman4india.f22labs.ApiServices;
import com.aman4india.f22labs.R;
import com.aman4india.f22labs.adapters.RecyclerAdapter;
import com.aman4india.f22labs.models.ItemsResponse;
import com.aman4india.f22labs.viewmodel.TextViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String BASE_URL = "https://android-full-time-task.firebaseio.com/";
    private static final String TAG = MainActivity.class.getSimpleName();
    public static TextView updatecart;
    RecyclerView recyclerView;
    ProgressBar progressbar;
    Toolbar toolbar;
    TextViewModel textViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        progressbar = findViewById(R.id.progressbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("FoOdErS");

        textViewModel = ViewModelProviders.of(this).get(TextViewModel.class);


        progressbar.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        getItemsDetails();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (textViewModel != null)
            textViewModel.getTotalCount().observe(this, new Observer<Integer>() {
                @Override
                public void onChanged(@Nullable Integer integer) {
                    if (updatecart != null)
                        updatecart.setText("" + integer);
                }
            });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        final MenuItem notificationItem = menu.findItem(R.id.action_cart);

        View notificationView = notificationItem.getActionView();
        Log.e(TAG, "onCreateOptionsMenu: " + notificationView);

        RelativeLayout menu_cart = notificationView.findViewById(R.id.menu_cart);
        ImageView imagecart = notificationView.findViewById(R.id.actionbar_notification_cart);
        updatecart = notificationView.findViewById(R.id.actionbar_notifcation_textview);
        if (textViewModel != null)
            textViewModel.getTotalCount().observe(this, new Observer<Integer>() {
                @Override
                public void onChanged(@Nullable Integer integer) {
                    if (updatecart != null)
                        updatecart.setText("" + integer);
                }
            });
        imagecart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(notificationItem);
            }
        });
        updatecart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(notificationItem);
            }
        });
        menu_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(notificationItem);
            }
        });


        return true;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_cart:
//                Toast.makeText(getApplicationContext(),"Item 1 Selected",Toast.LENGTH_LONG).show();
                if (updatecart!=null && (updatecart.getText().toString().equals("0") || updatecart.getText().toString().equals(""))){
                    Toast.makeText(this, "Your cart is empty", Toast.LENGTH_SHORT).show();
                    return false;
                }
                Intent intent = new Intent(MainActivity.this, CartActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getItemsDetails() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiServices apiServices = retrofit.create(ApiServices.class);

        Call<List<ItemsResponse>> responseCall = apiServices.getItemsResponse();
        try {
            progressbar.setVisibility(View.VISIBLE);
            responseCall.enqueue(new Callback<List<ItemsResponse>>() {
                @Override
                public void onResponse(Call<List<ItemsResponse>> call, Response<List<ItemsResponse>> response) {
                    Log.e(TAG, "onResponse: " + response.isSuccessful());
                    progressbar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        RecyclerAdapter adapter = new RecyclerAdapter(getApplication(),MainActivity.this, response.body(), textViewModel);
                        recyclerView.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<List<ItemsResponse>> call, Throwable t) {
                    progressbar.setVisibility(View.GONE);
                    Log.e(TAG, "onFailure: ", t);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "getItemsDetails: ", e);
            progressbar.setVisibility(View.GONE);
        }


    }


}
