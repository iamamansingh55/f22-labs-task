package com.aman4india.f22labs.activities;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aman4india.f22labs.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class DetailsActivity extends AppCompatActivity {

    ViewGroup backgroundGroup;
    ImageView itemimage;
    TextView nametitle, pricetitle, pricetxt, nametxt, ratingtxt;
    String name, image, price, rate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        backgroundGroup = findViewById(R.id.main_background);
        itemimage = findViewById(R.id.itemimage);
        pricetitle = findViewById(R.id.pricetitle);
        pricetxt = findViewById(R.id.pricetxt);
        nametitle = findViewById(R.id.nametitle);
        nametxt = findViewById(R.id.nametxt);
        ratingtxt = findViewById(R.id.ratingtxt);

        name = getIntent().getStringExtra("name");
        image = getIntent().getStringExtra("image");
        price = getIntent().getStringExtra("price");
        rate = getIntent().getStringExtra("rate");


        loadRandomImage();
    }


    private void loadRandomImage() {
        Picasso.get().load(image)
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .error(R.drawable.ic_cloud_white_24dp)
//                .placeholder(R.drawable.ic_cloud_white_24dp)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        assert itemimage != null;
                        itemimage.setImageBitmap(bitmap);
                        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(Palette palette) {
                                Palette.Swatch darkMutedSwatch = palette.getDarkMutedSwatch();

                                if (darkMutedSwatch == null) {
                                    return;
                                }
                                backgroundGroup.setBackgroundColor(darkMutedSwatch.getRgb());
                                nametitle.setTextColor(darkMutedSwatch.getTitleTextColor());
                                pricetitle.setTextColor(darkMutedSwatch.getTitleTextColor());
                                nametxt.setTextColor(darkMutedSwatch.getBodyTextColor());
                                pricetxt.setTextColor(darkMutedSwatch.getBodyTextColor());
//                                ratingtxt.setTextColor(darkMutedSwatch.getBodyTextColor());
                            }
                        });
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

    }


}
