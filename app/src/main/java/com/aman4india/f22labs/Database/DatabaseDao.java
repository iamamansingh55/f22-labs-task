package com.aman4india.f22labs.Database;
/*
 * Created by Santosh on 10-08-2018.
 */

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aman4india.f22labs.Database.DbText;

import java.util.List;


@Dao
public interface DatabaseDao {

    @Query("SELECT * FROM DbText")
    LiveData<List<DbText>> getAllTexts();

    @Query("SELECT qty FROM DbText where itemName = :itemname ")
    String  getQuantity(String itemname);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DbText text);

    @Query("SELECT COUNT(Distinct itemName) FROM DbText")
    LiveData<Integer> getTotalCount();


    @Query("DELETE FROM DbText where itemName = :itemname")
    void deleteItem(String  itemname);

    @Update
    void update(DbText word);

    @Query("UPDATE DbText SET qty=:qty WHERE itemName = :itemname")
    void update(String qty, String itemname);


    @Query("SELECT * FROM dbtext WHERE itemName = :title LIMIT 1")
    DbText findItemsByTitle(String title);



}