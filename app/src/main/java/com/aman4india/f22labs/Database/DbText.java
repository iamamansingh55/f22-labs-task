package com.aman4india.f22labs.Database;

/*
 * Created by Santosh on 10-08-2018.
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "DbText")
public class DbText {
    @PrimaryKey()
    @NonNull
//    @PrimaryKey(autoGenerate = true)
//    public int id;
    public String itemName;
    private String price;
    public String qty;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String rating) {
        this.qty = rating;
    }


}