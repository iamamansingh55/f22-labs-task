package com.aman4india.f22labs.Database;
/*
 * Created by Santosh on 10-08-2018.
 */

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;


@Database(entities = {DbText.class}, version = 3)
public abstract class CartDatabase extends RoomDatabase {

    private static CartDatabase INSTANCE;
    private static final String DB_NAME = "cart.db";

    public static CartDatabase getDatabase(final Context context) {

        if (INSTANCE == null) {
            synchronized (CartDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CartDatabase.class, DB_NAME)
                            .allowMainThreadQueries() // SHOULD NOT BE USED IN PRODUCTION !!!
                            .addCallback(new RoomDatabase.Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                }
                            })
                            .build();
                }
            }
        }

        return INSTANCE;
    }

    public abstract DatabaseDao textDao();




}