package com.aman4india.f22labs.viewmodel;

/*
 * Created by Santosh on 10-08-2018.
 */

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.aman4india.f22labs.Database.DbText;
import com.aman4india.f22labs.repository.TextRepository;

import java.util.List;


public class TextViewModel extends AndroidViewModel {
    private TextRepository mRepository;
    private LiveData<List<DbText>> mAllTexts;
    private LiveData<Integer> totalCount;

    public TextViewModel(Application application) {
        super(application);
        mRepository = new TextRepository(application);
        mAllTexts = mRepository.listLiveData();
        totalCount = mRepository.getTotalCount();
    }


    public LiveData<Integer> getTotalCount() {
        return totalCount;
    }

    public LiveData<List<DbText>> getAllTexts() {
        return mAllTexts;
    }

    public void insert(DbText word) {
        mRepository.insert(word);
    }

    public void deleteItem(DbText item) {
        mRepository.deleteItem(item);
    }

    public void updateItem(DbText item) {
        mRepository.updateItem(item);
    }



}