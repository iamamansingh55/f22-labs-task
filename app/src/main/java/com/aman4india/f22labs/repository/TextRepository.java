package com.aman4india.f22labs.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.aman4india.f22labs.Database.CartDatabase;
import com.aman4india.f22labs.Database.DatabaseDao;
import com.aman4india.f22labs.Database.DbText;

import java.util.List;


/*
 * Created by Santosh on 10-08-2018.
 */
public class TextRepository {

    private DatabaseDao mTextDao;
    private LiveData<List<DbText>> mAllTexts;
    private LiveData<Integer> totalCount;
    private LiveData<Integer> totalQty;

    public TextRepository(Application application) {
        CartDatabase db = CartDatabase.getDatabase(application);
        mTextDao = db.textDao();
        mAllTexts = mTextDao.getAllTexts();
        totalCount = mTextDao.getTotalCount();
    }



    public LiveData<List<DbText>> listLiveData() {
        return mAllTexts;
    }

    public void insert(DbText word) {
        new insertAsyncTask(mTextDao).execute(word);
    }

    public LiveData<Integer> getTotalCount(){
        return totalCount;
    }

    public void deleteItem(DbText word)  {
        new deleteAsyncTask(mTextDao).execute(word);
    }

    public void updateItem(DbText word)  {
        new updateAsyncTask(mTextDao).execute(word);
    }

    public LiveData<Integer> getQtyItem(DbText word)  {
        return totalQty;
    }


    private static class insertAsyncTask extends AsyncTask<DbText, Void, Void> {
        private DatabaseDao mAsyncTaskDao;

        insertAsyncTask(DatabaseDao wordDao) {
            this.mAsyncTaskDao = wordDao;
        }

        @Override
        protected Void doInBackground(DbText... words) {
            mAsyncTaskDao.insert(words[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<DbText, Void, Void> {
        private DatabaseDao mAsyncTaskDao;

        deleteAsyncTask(DatabaseDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(DbText... params) {
            mAsyncTaskDao.deleteItem(params[0].getItemName());
            return null;
        }
    }


    private static class updateAsyncTask extends AsyncTask<DbText, Void, Void> {
        private DatabaseDao mAsyncTaskDao;

        updateAsyncTask(DatabaseDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final DbText... params) {
            mAsyncTaskDao.update(params[0].getQty(),params[0].getItemName());
            return null;
        }
    }




}